<?php

namespace App\Repository;

use App\Entity\DescriptionUrl;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DescriptionUrl|null find($id, $lockMode = null, $lockVersion = null)
 * @method DescriptionUrl|null findOneBy(array $criteria, array $orderBy = null)
 * @method DescriptionUrl[]    findAll()
 * @method DescriptionUrl[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DescriptionUrlRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DescriptionUrl::class);
    }

    // /**
    //  * @return DescriptionUrl[] Returns an array of DescriptionUrl objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DescriptionUrl
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
