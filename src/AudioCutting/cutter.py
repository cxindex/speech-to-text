#!/usr/bin/env python
from pydub import AudioSegment
import sys
import ntpath
import os
ntpath.basename("a/b/c")

timestampStart=sys.argv[2]
timestampStart=timestampStart.replace('PT','')

if "H" in timestampStart:
    hours=timestampStart.split('H')[0]
    minutes=timestampStart.split('M')[0].split('H')[1]
    seconds=timestampStart.split('S')[0].split('M')[1]
    start=int(hours)*3600+int(minutes)*60+float(seconds)
elif "M" in timestampStart:
    minutes=timestampStart.split('M')[0]
    seconds=timestampStart.split('S')[0].split('M')[1]
    start=int(minutes)*60+float(seconds)
else:
    start=timestampStart.replace('S','')


timestampStop=sys.argv[3]
timestampStop=timestampStop.replace('PT','')

if "H" in timestampStop:
    hours=timestampStop.split('H')[0]
    minutes=timestampStop.split('M')[0].split('H')[1]
    seconds=timestampStop.split('S')[0].split('M')[1]
    stop=int(hours)*3600+int(minutes)*60+float(seconds)
elif "M" in timestampStop:
    minutes=timestampStop.split('M')[0]
    seconds=timestampStop.split('S')[0].split('M')[1]
    stop=int(minutes)*60+float(seconds)
else:
    stop=timestampStop.replace('S','')

fullFilePath=sys.argv[1]

if (len(sys.argv)-1)==4:
    partialName=sys.argv[4]
else:
    partialName=''

def path_leaf(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

splited=os.path.splitext(fullFilePath)

filename=path_leaf(splited[0])
extension=splited[1]

t1= float(start)
t2= float(stop)
t1 = t1 * 1000 #Works in milliseconds
t2 = t2 * 1000

#print('/var/www/html/speech-to-text/vendor/cutter/cutted/'+filename+partialName+extension)

newAudio = AudioSegment.from_wav(fullFilePath)
newAudio = newAudio[t1:t2]
newAudio.export('/var/www/html/speech-to-text/src/AudioFiles/cutted/'+filename+'-'+partialName+extension, format="wav") #Exports to a wav file in the current path.

#Getting full file path with extension, name and path
#Starting timestamp
#Ending timestamp

#trim timestamps to seconds
#convert timestamps to miliseconds
#get seperately filename, extension and path

#get timestamps from genesys
#queue files to convert (array with data)
#make a cut every 5 seconds
#new file should be named with the metric for example (interactionID)-(NPS), (interactionID)-(CES)
#if there is no multiple timestamps, new file will have the same name