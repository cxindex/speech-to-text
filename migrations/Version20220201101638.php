<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220201101638 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE records (id INT AUTO_INCREMENT NOT NULL, comment VARCHAR(255) DEFAULT NULL, sentiment NUMERIC(5, 3) DEFAULT NULL, interaction_id VARCHAR(255) DEFAULT NULL, file_name VARCHAR(255) DEFAULT NULL, partial_name VARCHAR(255) DEFAULT NULL, phrases LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', categories LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', client_name VARCHAR(255) DEFAULT NULL, date_added DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE records');
    }
}
