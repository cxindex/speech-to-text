<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TestCommand extends Command
{
    protected static $defaultName = 'continue-transcription';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure(): void
    {
        $this
        ->addArgument('infinite', InputArgument::OPTIONAL, 'Infinite loop')
        ->addArgument('env', InputArgument::OPTIONAL, '<dev|prod>')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $infinite = $input->getArgument('infinite');

        if ($infinite) {
            $io->note(sprintf('You passed an argument: %s', $infinite));
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
