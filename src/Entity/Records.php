<?php

namespace App\Entity;

use App\Repository\RecordsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecordsRepository::class)]
class Records
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $comment;

    #[ORM\Column(type: 'decimal', precision: 5, scale: 3, nullable: true)]
    private $sentiment;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $interaction_id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $file_name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $partial_name;

    #[ORM\Column(type: 'json', nullable: true)]
    private $phrases = [];

    #[ORM\Column(type: 'json', nullable: true)]
    private $categories = [];

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $client_name;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $date_added;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $provider;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSentiment(): ?string
    {
        return $this->sentiment;
    }

    public function setSentiment(?string $sentiment): self
    {
        $this->sentiment = $sentiment;

        return $this;
    }

    public function getInteractionId(): ?string
    {
        return $this->interaction_id;
    }

    public function setInteractionId(?string $interaction_id): self
    {
        $this->interaction_id = $interaction_id;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->file_name;
    }

    public function setFileName(?string $file_name): self
    {
        $this->file_name = $file_name;

        return $this;
    }

    public function getPartialName(): ?string
    {
        return $this->partial_name;
    }

    public function setPartialName(?string $partial_name): self
    {
        $this->partial_name = $partial_name;

        return $this;
    }

    public function getPhrases(): ?array
    {
        return $this->phrases;
    }

    public function setPhrases(?array $phrases): self
    {
        $this->phrases = $phrases;

        return $this;
    }

    public function getCategories(): ?array
    {
        return $this->categories;
    }

    public function setCategories(?array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getClientName(): ?string
    {
        return $this->client_name;
    }

    public function setClientName(?string $client_name): self
    {
        $this->client_name = $client_name;

        return $this;
    }

    public function getDateAdded(): ?\DateTimeInterface
    {
        return $this->date_added;
    }

    public function setDateAdded(?\DateTimeInterface $date_added): self
    {
        $this->date_added = $date_added;

        return $this;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function setProvider(?string $provider): self
    {
        $this->provider = $provider;

        return $this;
    }
}
